Rails.application.routes.draw do
  mount Peek::Railtie => '/peek'

  require 'sidekiq/web'
  mount Sidekiq::Web => '/admin/sidekiq'

  namespace :admin do
    resources :identities
    resources :videos
    resources :lyrics
    resources :track_providers
    resources :playlists
    resources :video_providers
    resources :tracks
    resources :users

    root to: "identities#index"
  end

  match '/auth/demo', to: 'sessions#demo', via: :get, as: :demo
  match '/auth/:provider/callback', to: 'sessions#create', via: [:get, :post]
  match '/auth/failure', to: 'errors#login_failed', via: [:get, :post]
  match '/logout', to: 'sessions#destroy', via: [:get, :post], as: :logout

  # You can have the root of your site routed with "root"
  root 'landing#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  resources :playlists, only: :index do
    resources :tracks, only: :show
  end

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
