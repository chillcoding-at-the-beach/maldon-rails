OmniAuth.config.logger = Rails.logger

Rails.application.config.middleware.use OmniAuth::Builder do
  provider :deezer, ENV['DEEZER_APP_ID'], ENV['DEEZER_APP_SECRET'], perms: 'basic_access,email,offline_access'
  provider :spotify, ENV['SPOTIFY_APP_ID'], ENV['SPOTIFY_APP_SECRET'], scope: 'playlist-read-private user-read-private user-read-email'
end
