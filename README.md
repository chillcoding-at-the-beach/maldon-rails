# Maldon

## Getting Started

This repository comes equipped with a self-setup script:

```shell
% ./bin/setup
```

After setting up, you can run the application using [foreman]:

```shell
% foreman start -f Procfile.dev
```

[foreman]: http://ddollar.github.io/foreman/

## Deployment

```shell
git push staging|production
```

## Philosophy

### Models

#### Song providers (SP)

- Deezer
- Spotify
- LastFM (Not yet supported)
- YouTube (Not yet supported)

#### Video providers (VP)

- Dailymotion
- YouTube

### Associations

```mermaid
classDiagram
  User "1" --o "0..*" Identity : has many
  Identity "1" --o "0..*" Playlist : has many
  TrackProvider "1" --o "0..*" Identity : has many
  TrackProvider "1" <-- "0..*" Track : belongs to
  VideoProvider "1" --o "0..*" Video : has many
  Track "1..*" o-- "1" Video : has many
  Track "1" --* "1" Lyric : has one
  Playlist "1..*" --o "0..*" Track : has and belongs to many
```

## Challenges

### How to have a fast website when we have almost no matches between songs and videos or when we don't have the visitor's playlists cached?

1. Look for songs and videos in the background (Sidekiq jobs) and push updates to the front (Action Cable + Batman.js).

- Fetch/retrieve playlists and store them in DB (+ memcache) -> push playlists to front
- Then fetch/retrieve songs (potentially already with matches) for first playlist and store them in DB (+ memcache) -> push songs to playlist on front
- Then fetch/retrieve songs (potentially already with matches) for other playlists and store them in DB (+ memcache) -> push songs to playlists on front
- In parallel, fetch matches between songs and videos and store them in DB (+ memcache) -> update songs on front

### How to refresh cached infos (mostly playlists) so that they're as much as possible in sync with song providers?

1. Manual "refresh" button
2. Periodic check of most player playlists
  - We'll have to track each time a playlist is loaded and each time a song is played.

## Next steps

- Consolidate songs between providers (same song on different providers will have the same video)

## Guidelines

Use the following guides for getting things done, programming well, and
programming in style.

* [Protocol](http://github.com/thoughtbot/guides/blob/master/protocol)
* [Best Practices](http://github.com/thoughtbot/guides/blob/master/best-practices)
* [Style](http://github.com/thoughtbot/guides/blob/master/style)
