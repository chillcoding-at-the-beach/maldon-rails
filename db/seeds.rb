# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
User.demo_user
TrackProvider.create!(name: 'deezer')
TrackProvider.create!(name: 'spotify')
VideoProvider.create!(name: 'dailymotion')
VideoProvider.create!(name: 'youtube')
