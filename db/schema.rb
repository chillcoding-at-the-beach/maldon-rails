# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2014_05_30_215544) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "identities", force: :cascade do |t|
    t.bigint "track_provider_id", null: false
    t.bigint "user_id", null: false
    t.string "uid", null: false
    t.text "access_token", null: false
    t.datetime "signed_up_at"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["track_provider_id", "user_id"], name: "index_identities_on_track_provider_id_and_user_id", unique: true
    t.index ["track_provider_id"], name: "index_identities_on_track_provider_id"
    t.index ["user_id"], name: "index_identities_on_user_id"
  end

  create_table "lyrics", force: :cascade do |t|
    t.bigint "track_id", null: false
    t.text "payload", null: false
    t.integer "offset", default: 0, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["track_id"], name: "index_lyrics_on_track_id"
  end

  create_table "playlists", force: :cascade do |t|
    t.bigint "track_provider_id", null: false
    t.bigint "identity_id"
    t.string "ref", null: false
    t.string "title", null: false
    t.text "description", default: ""
    t.integer "duration", default: 0
    t.string "original_url", default: "", null: false
    t.string "picture_url", default: "", null: false
    t.integer "access", null: false
    t.boolean "default", default: false, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["identity_id"], name: "index_playlists_on_identity_id"
    t.index ["track_provider_id", "ref"], name: "index_playlists_on_track_provider_id_and_ref", unique: true
    t.index ["track_provider_id"], name: "index_playlists_on_track_provider_id"
  end

  create_table "playlists_tracks", id: false, force: :cascade do |t|
    t.bigint "playlist_id", null: false
    t.bigint "track_id", null: false
    t.index ["playlist_id", "track_id"], name: "index_playlists_tracks_on_playlist_id_and_track_id", unique: true
    t.index ["playlist_id"], name: "index_playlists_tracks_on_playlist_id"
    t.index ["track_id"], name: "index_playlists_tracks_on_track_id"
  end

  create_table "track_providers", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_track_providers_on_name", unique: true
  end

  create_table "tracks", force: :cascade do |t|
    t.bigint "track_provider_id", null: false
    t.bigint "video_id"
    t.string "ref", null: false
    t.string "artist_name", default: ""
    t.string "artist_image_url", default: ""
    t.string "title", null: false
    t.string "isrc", default: ""
    t.string "original_url", default: ""
    t.string "album_title", default: ""
    t.string "album_image_url", default: ""
    t.integer "duration", default: 0
    t.boolean "explicit_lyrics", default: false
    t.integer "bpm", default: 0
    t.integer "reports", default: 0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["reports"], name: "index_tracks_on_reports"
    t.index ["track_provider_id", "ref"], name: "index_tracks_on_track_provider_id_and_ref", unique: true
    t.index ["track_provider_id"], name: "index_tracks_on_track_provider_id"
    t.index ["video_id"], name: "index_tracks_on_video_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "email", null: false
    t.string "login", null: false
    t.string "first_name", default: ""
    t.string "last_name", default: ""
    t.integer "gender", default: 0
    t.text "picture_url", default: ""
    t.string "country", limit: 2
    t.string "lang", limit: 2
    t.date "birthday"
    t.boolean "admin", default: false, null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["admin"], name: "index_users_on_admin"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["login"], name: "index_users_on_login"
  end

  create_table "video_providers", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["name"], name: "index_video_providers_on_name", unique: true
  end

  create_table "videos", force: :cascade do |t|
    t.bigint "video_provider_id", null: false
    t.string "ref", null: false
    t.string "title", null: false
    t.float "aspect_ratio", default: 0.0
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
    t.index ["video_provider_id", "ref"], name: "index_videos_on_video_provider_id_and_ref", unique: true
    t.index ["video_provider_id"], name: "index_videos_on_video_provider_id"
  end

end
