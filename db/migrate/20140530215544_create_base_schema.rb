class CreateBaseSchema < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.string  :email, null: false
      t.string  :login, null: false
      t.string  :first_name, null: true, default: ''
      t.string  :last_name, null: true, default: ''
      t.integer :gender, null: true, default: 0
      t.text    :picture_url, null: true, default: ''
      t.string  :country, null: true, limit: 2
      t.string  :lang, null: true, limit: 2
      t.date    :birthday, null: true
      t.boolean :admin, null: false, default: false

      t.timestamps null: false

      t.index :email, unique: true
      t.index :login
      t.index :admin
    end

    create_table :identities do |t|
      t.references  :track_provider, null: false
      t.references  :user, null: false
      t.string      :uid, null: false
      t.text        :access_token, null: false
      t.datetime    :signed_up_at, null: true

      t.timestamps null: false

      t.index [:track_provider_id, :user_id], unique: true
    end

    create_table :track_providers do |t|
      t.string :name, null: false

      t.timestamps null: false

      t.index :name, unique: true
    end

    create_table :video_providers do |t|
      t.string :name, null: false

      t.timestamps null: false

      t.index :name, unique: true
    end

    create_table :tracks do |t|
      t.references  :track_provider, null: false
      t.references  :video, null: true
      t.string      :ref, null: false
      t.string      :artist_name, null: true, default: ''
      t.string      :artist_image_url, null: true, default: ''
      t.string      :title, null: false
      t.string      :isrc, null: true, default: ''
      t.string      :original_url, null: true, default: ''
      t.string      :album_title, null: true, default: ''
      t.string      :album_image_url, null: true, default: ''
      t.integer     :duration, null: true, default: 0
      t.boolean     :explicit_lyrics, null: true, default: false
      t.integer     :bpm, null: true, default: 0
      t.integer     :reports, null: true, default: 0

      t.timestamps null: false

      t.index :reports
      t.index [:track_provider_id, :ref], unique: true
    end

    create_table :videos do |t|
      t.references  :video_provider, null: false
      t.string      :ref, null: false
      t.string      :title, null: false
      t.float       :aspect_ratio, null: true, default: 0

      t.timestamps null: false

      t.index [:video_provider_id, :ref], unique: true
    end

    create_table :lyrics do |t|
      t.references  :track, null: false
      t.text        :payload, null: false
      t.integer     :offset, null: false, default: 0 # offset in millisecond

      t.timestamps null: false
    end

    create_table :playlists do |t|
      t.references  :track_provider, null: false
      t.references  :identity, null: true
      t.string      :ref, null: false
      t.string      :title, null: false
      t.text        :description, null: true, default: ''
      t.integer     :duration, null: true, default: 0
      t.string      :original_url, null: false, default: ''
      t.string      :picture_url, null: false, default: ''
      t.integer     :access, null: false # enum: ['pri', 'pub']
      t.boolean     :default, null: false, default: false

      t.timestamps null: false

      t.index [:track_provider_id, :ref], unique: true
    end

    create_join_table :playlists, :tracks do |t|
      t.index :playlist_id
      t.index :track_id
      t.index [:playlist_id, :track_id], unique: true
    end
  end
end
