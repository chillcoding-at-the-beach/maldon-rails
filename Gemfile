source 'https://rubygems.org'

ruby '2.6.6'

## SERVER ##
gem 'puma'

group :staging, :production do
  gem 'newrelic_rpm', '>= 3.7.3'
end

group :production do
  gem 'puma-heroku'
  gem 'rails_12factor'
end

## CORE ##
gem 'rails', '6.0.2.2'
gem 'sidekiq'
gem 'bootsnap', require: false
gem 'rack-timeout'
gem 'pg'
gem 'oj', '~> 3.10' # Faster JSON

## OAuth providers ##
gem 'omniauth-deezer'
gem 'omniauth-spotify'

## Async work ##
group :job do
  gem 'sucker_punch', '~> 2.1'
end
gem 'httparty'

gem 'jbuilder', '~> 2.10'
gem 'recipient_interceptor'
gem 'responders'
gem 'ransack'

## ADMIN ##
gem 'administrate'

## FRONT ##
gem 'webpacker', '~> 5.1'
gem 'sass-rails', '~> 6.0'
gem 'coffee-rails', '~> 5.0'
gem 'jquery-rails'
gem 'flight-for-rails', '~> 1.5'
gem 'turbolinks'
# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'uglifier', '~> 4.2'

## DEV ##
gem 'peek'
gem 'peek-dalli'
gem 'peek-gc'
gem 'peek-git'
gem 'peek-performance_bar'
gem 'peek-pg'

group :development, :test do
  gem 'awesome_print'
  gem 'dotenv-rails'
end

group :development do
  gem 'listen'
  gem 'rails-erd', require: false
  gem 'brakeman', require: false
  gem 'pry-rails'
  gem 'annotate'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'foreman'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'guard-minitest'
  gem 'rubocop', require: false
end

group :test do
  gem 'simplecov', require: false
end

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', '~> 0.4.0'
end
