# This is a manifest file that'll be compiled into application.js, which will include all the files
# listed below.
#
# Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
# or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
#
# It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
# compiled file.
#
# Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
# about supported directives.
#
# require jquery
# require jquery_ujs
# require turbolinks
# require flight-for-rails
# require peek
#= require_self
# require peek/views/performance_bar # error with $.fn.tipsy.autoNS
# require_tree ./components
# require_tree ./players
# require_tree .

window.Maldon =
  Players: {}

# $ ->
#   $(window).on 'beforeunload unload', =>
#     @trigger document, 'PAUSE_REQUESTED'

  # Maldon.Player = new Maldon.Players.Dailymotion('player-container', 'player')

  # if $('.landing-page')?
  #   $track = $('.track.selected')
  #   Maldon.Player.loadVideo($track.data('video-id'),
  #     mute: 1
  #     loop: 1
  #   )
  #
  #   $('body').on 'click', '.connect', (e) ->
  #     e.preventDefault()
  #     Maldon.Player.pause()
  #     window.location = URL + 'auth/' + $(this).data('service')
  #
  #     no

  # if $('.playlists-page')?
  #   # Maldon.Menu     = new Maldon.UI.Menu($('#menu'), $('#main-view'))
  #   # Maldon.Controls = new Maldon.UI.Controls($('#player-overlay'), Maldon.Player)
