function init() {
  playerwrapper.automute = false;
  var $firstVideo = function() {
    if ((selected = $('.track.selected')).length > 0)
    {
      return selected;
    }
    else
    {
      return $('.track').first();
    }
  }();

  // playerwrapper.load($firstVideo);

  updateLyricsIconAndPlaceholder();
  updateVolumeIcon();
  // $(window).on('resize', function() {
  //   var usedSpace = $('.menu-top').height() + $('.menu-tools').height() + $('.menu-cover').height();
  //   var freeSpace = $('#menu').height() - usedSpace;
  //   $('#menu .menu-slide').height(freeSpace);
  // });

  $('body').on('click', '.icon-cw', function() {
    $(this).toggleClass('selected');
    playerwrapper.loop == 1 ? playerwrapper.loop = 0 : playerwrapper.loop = 1;
  });

  $('body').on('click', '.icon-shuffle2', function() {
    $(this).toggleClass('selected');
    playerwrapper.shuffle();
  });

  $('body').on('click', '.icon-loop', function() {
    $(this).toggleClass('selected');
    playerwrapper.random == 1 ? playerwrapper.random = 0 : playerwrapper.random = 1;
  });

  $('body').on('click', '.icon-microphone', function() {
    playerwrapper.toggleLyrics();
    updateLyricsIconAndPlaceholder();
  });

  $(window).trigger('resize');

  setTimeout(function()
  {
    playerwrapper.forwardMenu();
  }, 1200);

  $(window).on('mousemove click touch touchstart keyup keydown', function()
  {
    playerwrapper.resetTimeout();
  });

  $('body').on('keyup', '.search input', function()
  {
    var value = $(this).val();
    var regex = new RegExp(value,'gi');
    //console.log(regex);
    $('.menu-slide.playlists li').each(function()
    {
      //console.log($(this).data('title'));
      if (!$(this).data('title').match(regex))
      {
        $(this).hide();
      }
      else {
        $(this).show();
      }
    });
  });

  $('#player-overlay,.playButton').click(function()
  {
    playerwrapper.playToggle();
  });

  $('body').on('click', '.icon-first', function()
  {
    playerwrapper.prevTrack();
  });

  $('body').on('click', '.icon-last', function()
  {
    playerwrapper.nextTrack();
  });

  playerwrapper.closeMenu = function()
  {
    $('#main-view,#menu').addClass('menu-closed');
    $('#player-controls').addClass('inactive');
  };

  playerwrapper.openMenu = function()
  {
    $('#main-view,#menu').removeClass('menu-closed');
  };

  playerwrapper.toggleMenu = function()
  {
    if ($('#main-view,#menu').hasClass('menu-closed'))
    {
      this.openMenu();
    }
    else
    {
      this.closeMenu();
    }
  };

  $('body').on('click', '.menu-expand', function()
  {
    playerwrapper.closeMenu();
  });

  $('body').on('click', '#main-view.menu-closed .menu-icon', function()
  {
    playerwrapper.openMenu();
  });

  $('body').on('click', '.track', function(e)
  {
    e.preventDefault();
    if ($(this).hasClass('disabled'))
    {
      // Sorry track unavailable message ?
      return;
    }
    if ($(this).hasClass('selected'))
    {
      playerwrapper.pause();
      return;
    }

    history.pushState(
      {},
      '',
      '/playlists/'+$(this).data('track-provider')[0]+$(this).data('playlist-id')+'/tracks/'+$(this).data('track-id')
    );

    playerwrapper.load($(this));
    $('.cover img').attr('src',$(this).data('album-picture'));
  });

  $('body').on('click', '.report', function(e)
  {
    e.preventDefault();

    if ($('.track.selected').length)
    {
      var $track = $('.track.selected');
      $.post(URL + 'video/report', { provider: $track.data('track-provider'), track_id: $track.data('track-id') })
      .done(function(data)
      {
        console.log("Reported: " + $track.data('track-provider') + '/' + $track.data('track-id') + ': ' + data.reports + ' reports!');
      });
      playerwrapper.nextTrack();
    }
  });

  function afterPlaylistLoad($elem)
  {
    if (typeof $elem !== "undefined")
    {
      $elem.find('span').remove();
    }

    $('.menu-cover-wrapper').animate({ left: '-350px' }, 560, function()
    {
      $('.menu-current').first().remove();
      $('.menu-cover-wrapper').css('left',0);
    });
    playerwrapper.forwardMenu();
  }

  $('body').on('click', '.playlist', function(e)
  {
    if ($(this).find('.spin').length || $(this).data('tracks-count') === 0) return;
    e.preventDefault();
    // Preview cover handling
    $newPreview = $('.menu-current').clone().appendTo('.menu-cover-wrapper');
    $newPreview.find('.cover img').attr('src',$(this).data('album-picture'));
    $newPreview.find('.cover-overlay img').attr('src',$(this).data('img'));
    $newPreview.find('.title').html($(this).data('title'));
    var duration = moment.duration({'seconds':parseInt($(this).data('duration'))}).humanize();
    $newPreview.find('.duration').html(duration);
    $newPreview.find('.tracks-count').html($(this).data('tracks-count'));

    var id = $(this).data('id');

    history.pushState(
      {},
      '',
      '/playlists/'+$(this).data('provider')[0]+$(this).data('id')
    );

    $(this).prepend('<span class="spin"><i class="icon-spinner3"></i></span>');
    var $that =  $(this);
    if (playlistwrapper.has(id))
    {
      //We got data already
      // Build menu from it !
      playlistwrapper.buildMenu(id);
      afterPlaylistLoad($that);
    }
    else {
      // No data, get it, then build
      $.getJSON(URL + 'home/playlist/' + id, function(json)
      {
        playlistwrapper.fillPlaylist(json);
        playlistwrapper.buildMenu(id);
        afterPlaylistLoad($that);
      }).always(function()
      {
        $that.find('span').hide(function(){ $(this).remove(); });
      });
    }

  });

  playerwrapper.backMenu = function()
  {
    $('#menu .menu-slide').css('left', '0px');
    $('.menu-slide').removeClass('tracks').addClass('playlists');
    $('.menu-tools').removeClass('track-panel').addClass('playlist-panel');
  }

  playerwrapper.forwardMenu = function()
  {
    // $('#menu .menu-slide').css({ left: -$('#menu .menu-slide').width()/2 + 'px', height: '100%' });
    $('#menu .menu-slide').removeClass('playlists').addClass('tracks');
    $('#menu .menu-tools').removeClass('playlist-panel').addClass('track-panel');
    $('#menu .menu-slide').css('height', '100%');
  }

  playerwrapper.prevTrack = function()
  {
    if (!$('.track.selected').length)
    {
      $('.track').first().click();
      return;
    }
    if ($('.track.selected').prevAll('.track:not(.track.disabled)').first().length)
    {
      $('.track.selected').prevAll('.track:not(.track.disabled)').first().click();
    }
  }

  playerwrapper.nextTrack = function()
  {
    if (!$('.track.selected').length)
    {
      $('.track').first().click();
      return;
    }
    if ($('.track.selected').nextAll('.track:not(.track.disabled)').first().length)
    {
      $('.track.selected').nextAll('.track:not(.track.disabled)').first().click();
    }
  }

  function updateLyricsIconAndPlaceholder()
  {
    // We rely only on the state returned by the playerwrapper to avoid
    // inconsistencies between playerwrapper and the UI...

    var $microphoneIcon = $('.icon-microphone');
    var iconIsOff = $microphoneIcon.hasClass('selected');
    if (playerwrapper.showLyrics)
    {
      if (iconIsOff) $microphoneIcon.removeClass('selected')
      $('#lyrics:hidden').fadeIn();
    }
    else
    {
      if (!iconIsOff) $microphoneIcon.addClass('selected')
      $('#lyrics:visible').fadeOut();
    }
  }
  function updateVolumeIcon()
  {
    // We rely only on the state returned by the playerwrapper to avoid
    // inconsistencies between playerwrapper and the UI...

    var $volumeIcon = $('.volume');
    var iconIsMuted = $volumeIcon.hasClass('icon-volume-mute');
    if (playerwrapper.automute)
    {
      if (!iconIsMuted) $volumeIcon.removeClass('icon-volume-medium').addClass('icon-volume-mute');
    }
    else
    {
      if (iconIsMuted) $volumeIcon.addClass('icon-volume-medium').removeClass('icon-volume-mute');
    }
  }

  $('body').on('click', '.menu-cover', function()
  {
    playerwrapper.forwardMenu();
  });

  $('body').on('click', '#go-to-previous', function()
  {
    playerwrapper.backMenu();
  });

  $('body').on('click', '#go-to-forward', function()
  {
    playerwrapper.forwardMenu();
  });

  $('.progress-bar').bind('click touch', function (ev)
  {
    var $div = $(ev.target);
    //var $display = $(ev.target);

    var offset = $div.offset();
    var x = ev.clientX - offset.left;
    var percent = (x / $(this).width());
    //console.log(percent,'% <--- togo');
    playerwrapper.seekProgress(percent);
  });

  $('.sound-bar').bind('click touch', function (ev)
  {
    var $div = $(ev.target);
    //var $display = $(ev.target);

    var offset = $div.offset();
    var x = ev.clientX - offset.left;
    var percent = (x / $(this).width()) *100;
    $('.sound-progress').width(percent + '%');
    // console.log(percent,'% <--- togo');
    playerwrapper.volume(percent);
  });

  $('.volume').on('click', function()
  {
    playerwrapper.toggleMute();
    updateVolumeIcon();
  });

  $(window).keydown(function(e){
    if (e.keyCode == 37 && $('.menu-slide').hasClass('tracks'))
    { // ARROW LEFT
      playerwrapper.backMenu();
      return false;
    }
    else if (e.keyCode == 39 && !$('.menu-slide').hasClass('tracks'))
    { // ARROW RIGHT
      playerwrapper.forwardMenu();
      return false;
    }
    else if (e.keyCode == 38 && $('.menu-slide').hasClass('tracks'))
    { // ARROW UP
      playerwrapper.prevTrack();
      return false;
    }
    else if (e.keyCode == 40 && $('.menu-slide').hasClass('tracks'))
    { // ARROW DOWN
      playerwrapper.nextTrack();
      return false;
    }
    else if (e.keyCode == 27)
    { // ESC
      playerwrapper.toggleMenu();
      return false;
    }
    else if (e.keyCode == 32)
    { // SPACE
      playerwrapper.playToggle();
      return false;
    }
    else if (e.keyCode == 76)
    { // l
      playerwrapper.toggleLyrics();
      updateLyricsIconAndPlaceholder();
      return false;
    }
    else if (e.keyCode == 77)
    { // m
      playerwrapper.toggleMute();
      updateVolumeIcon();
      return false;
    }
  });

  // console.log($().qtip);
  // console.log('Tipsying');
  $('[tooltip]').each(function()
  {
    var $that = $(this);
    $(this).qtip({
      content: {
        attr: 'tooltip'
      },
      position: {
        my: 'bottom center',
        at: 'top center',
        target: $that
      },
      style: 'qtip-tipsy'
    });
  });

  // var $karaoke = $('.karaoke-mode');
  // $karaoke.qtip({
  //     content: $('#templates .karaoke-menu'),
  //     prerender: true,
  //     position: {
  //         my: 'top left',
  //         at: 'bottom left',
  //         target: $karaoke,
  //         adjust: {
  //             y: -8,
  //             x: -1
  //         }
  //     },
  //     show: {
  //         delay: 0
  //     },
  //     hide: {
  //          fixed: true,
  //          delay: 30000000
  //      },
  //     style: 'qtip-karaoke'
  // });
}

$(function() {

  if ($('.home-page').length) {
    init();
  }

});
