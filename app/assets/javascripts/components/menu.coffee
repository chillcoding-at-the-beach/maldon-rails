$ ->
  # FIXME: Separate the menu from the actual playlist
  menu = flight.component ->

    @attributes
      menuCoverImgSelector: '.cover img'
      trackSelector: '.track'
      currentTrackSelector: '.track.selected'
      menuCoverSelector: '.menu-cover'
      menuSliderSelector: '.menu-slide'
      menuToolsSelector: '.menu-tools'
      menuTopSelector: '.menu-top'
      menuExpandSelector: '.menu-expand'
      menuReportSelector: '.report'
      menuLoopSelector: '.icon-cw'
      menuShuffleSelector: '.icon-shuffle2'
      menuLyricsSelector: '.icon-microphone'
      # FIXME
      # menuShuffleSelector: '.icon-loop'

    @after 'initialize', ->

      # Custom events listeners
      @on document, 'TOGGLE_MENU_REQUESTED', =>
        console.log '[menu] TOGGLE_MENU_REQUESTED event received!'
        @toggle()

      @on document, 'LOAD_REQUESTED', (event, data) =>
        console.log '[menu] LOAD_REQUESTED event received!'
        @select('menuCoverImgSelector').attr 'src', data.album_picture

      @on document, 'RESET_UI_AUTOHIDE_TIMEOUTS', @resetTimeout

      @select('menuExpandSelector').on 'click', =>
        @trigger('TOGGLE_MENU_REQUESTED')

      # Global listeners
      $(window).on 'resize', =>
        usedSpace = @select('menuTopSelector').height() + @select('menuToolsSelector').height() + @select('menuCoverSelector').height()
        freeSpace = @$node.height() - usedSpace
        @select('menuSliderSelector').height(freeSpace)

      # FIXME: Too many events!
      $(window).on 'mousemove click touch touchstart keyup keydown', =>
        @trigger('RESET_UI_AUTOHIDE_TIMEOUTS')

      menu = this
      $('body').on 'keyup', '.search input', ->
        value = $(this).val()
        regex = new RegExp(value, 'gi')
        # console.log(regex);
        menu.$slider.find('li').each ->
          # console.log($(this).data('title'));
          if $(this).data('title').match(regex)
            $(this).show()
          else
            $(this).hide()

      # Mouse / keyboard listeners
      @select('menuLoopSelector').on 'click', (event) =>
        $(event.currentTarget).toggleClass('selected')
        @trigger('TOGGLE_LOOP_REQUESTED')

      @select('menuShuffleSelector').on 'click', (event) =>
        $(event.currentTarget).toggleClass('selected')
        @trigger('TOGGLE_SHUFFLE_REQUESTED')

      @select('menuLyricsSelector').on 'click', (event) =>
        $(event.currentTarget).toggleClass('selected')
        @trigger('TOGGLE_LYRICS_REQUESTED')

      @select('trackSelector').on 'click', (event) =>
        event.preventDefault()
        @loadTrack($(event.currentTarget))

      @loadTrack = ($track) ->
        if $track.hasClass('disabled')
          # Sorry track unavailable message ?
          return

        if $track.hasClass('selected')
          @trigger('PAUSE_REQUESTED')
          return

        history.pushState {}, '', "/playlists/#{$track.data('track-provider')[0]}#{$track.data('playlist-id')}/tracks/#{$track.data('track-id')}"

        data =
          provider: $track.data('video-provider')
          videoId: $track.data('video-id')
          album_picture: $track.data('album-picture')
        @trigger('LOAD_REQUESTED', data)

      @select('menuReportSelector').on 'click', (event) =>
        event.preventDefault()
        $track = $(event.currentTarget)

        $currentTrack = @select('currentTrackSelector')
        if $currentTrack?
          $.post('/video/report', { provider: $currentTrack.data('track-provider'), track_id: $currentTrack.data('track-id') })
          .done((data) ->
            console.log("Reported: #{$currentTrack.data('track-provider')}/#{$currentTrack.data('track-id')}: #{data.reports} reports!")
          )
          playerwrapper.nextTrack()

      setTimeout(=>
        @forward()
      , 1000)

      $(window).trigger('resize')

    @resetTimeout = ->
      if @autoCloseMenuTimeout?
        clearTimeout(@autoCloseMenuTimeout)

      unless @$node.hasClass('menu-closed')
        @autoCloseMenuTimeout = setTimeout(=>
          @trigger('TOGGLE_MENU_REQUESTED')
        , 4500)

    @open = ->
      @$node.removeClass('menu-closed')

    @close = ->
      @$node.addClass('menu-closed')

    @toggle = ->
      if @$node.hasClass('menu-closed')
        @open()
      else
        @close()

    @forward = ->
      @select('menuSliderSelector').css('left', -@select('menuSliderSelector').width()/2 + 'px')
      @select('menuSliderSelector').removeClass('playlists').addClass('tracks')
      @select('menuToolsSelector').removeClass('playlist-panel').addClass('track-panel')
      @select('menuSliderSelector').find('.playlist.selected .tracks').removeAttr('style')

    @backward = ->
      @select('menuSliderSelector').css('left', '0px')
      @select('menuSliderSelector').removeClass('tracks').addClass('playlists')
      @select('menuToolsSelector').removeClass('track-panel').addClass('playlist-panel')

  menu.attachTo '#menu'
