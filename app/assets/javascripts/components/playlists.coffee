$ ->
  playlists = flight.component ->

    @attributes
      playlistSelector: '.playlist'
      currentPlaylistSelector: '.playlist.selected'
      currentPlaylistTrackSelector: '.playlist.selected .track'
      currentPlaylistCurrentTrackSelector: '.playlist.selected .track.selected'

    @after 'initialize', ->

      @on document, 'PREVIOUS_TRACK_REQUESTED', (event) =>
        console.log '[playlists] PREVIOUS_TRACK_REQUESTED event received!'
        data = null

        if ($currentTrack = @select('currentPlaylistCurrentTrackSelector')).length
          if $previousTrack = $currentTrack.prevAll('.track:not(.track.disabled)').first()
            data =
              provider: $previousTrack.data('video-provider')
              videoId: $previousTrack.data('video-id')
              album_picture: $previousTrack.data('album-picture')
        else
          $firstTrack = @select('currentPlaylistTrackSelector').first()
          data =
            provider: $firstTrack.data('video-provider')
            videoId: $firstTrack.data('video-id')
            album_picture: $firstTrack.data('album-picture')

        @trigger('LOAD_REQUESTED', data) if data?

      @on document, 'NEXT_TRACK_REQUESTED', (event) =>
        console.log '[playlists] NEXT_TRACK_REQUESTED event received!'
        data = null

        console.log "@select('currentPlaylistCurrentTrackSelector')", @select('currentPlaylistCurrentTrackSelector')

        if ($currentTrack = @select('currentPlaylistCurrentTrackSelector')).length
          console.log '$currentTrack', $currentTrack
          if $nextTrack = $currentTrack.nextAll('.track:not(.track.disabled)').first()
            console.log '$nextTrack', $nextTrack
            data =
              provider: $nextTrack.data('video-provider')
              videoId: $nextTrack.data('video-id')
              album_picture: $nextTrack.data('album-picture')
        else
          $firstTrack = @select('currentPlaylistTrackSelector').first()
          console.log '$firstTrack', $firstTrack
          data =
            provider: $firstTrack.data('video-provider')
            videoId: $firstTrack.data('video-id')
            album_picture: $firstTrack.data('album-picture')

        @trigger('LOAD_REQUESTED', data) if data?

    #   # Custom events listeners
    #   @on document, 'TOGGLE_MENU_REQUESTED', =>
    #     console.log '[menu] TOGGLE_MENU_REQUESTED event received!'
    #     @toggle()

    #   @on document, 'LOAD_REQUESTED', (event, data) =>
    #     console.log '[menu] LOAD_REQUESTED event received!'
    #     @select('menuCoverImgSelector').attr 'src', data.album_picture

    #   @on document, 'PREVIOUS_TRACK_REQUESTED', (event) =>
    #     console.log '[menu] PREVIOUS_TRACK_REQUESTED event received!'
    #     $currentTrack = @select('currentTrackSelector')
    #     unless $currentTrack?
    #       @loadTrack(@select('trackSelector').first())
    #       return

    #     @loadTrack($previousTrack) if $previousTrack = $currentTrack.prevAll('.track:not(.track.disabled)').first()

    #   @on document, 'NEXT_TRACK_REQUESTED', (event) =>
    #     console.log '[menu] NEXT_TRACK_REQUESTED event received!'
    #     $currentTrack = @select('currentTrackSelector')
    #     unless $currentTrack?
    #       @loadTrack(@select('trackSelector').first())
    #       return

    #     @loadTrack($previousTrack) if $previousTrack = $currentTrack.nextAll('.track:not(.track.disabled)').first()

    #   @on document, 'RESET_UI_AUTOHIDE_TIMEOUTS', @resetTimeout

    #   @select('menuExpandSelector').on 'click', =>
    #     @trigger('TOGGLE_MENU_REQUESTED')

    #   # Global listeners
    #   $(window).on 'resize', =>
    #     usedSpace = @select('menuTopSelector').height() + @select('menuToolsSelector').height() + @select('menuCoverSelector').height()
    #     freeSpace = @$node.height() - usedSpace
    #     @select('menuSliderSelector').height(freeSpace)

    #   # FIXME: Too many events!
    #   $(window).on 'mousemove click touch touchstart keyup keydown', =>
    #     @trigger('RESET_UI_AUTOHIDE_TIMEOUTS')

    #   menu = this
    #   $('body').on 'keyup', '.search input', ->
    #     value = $(this).val()
    #     regex = new RegExp(value, 'gi')
    #     # console.log(regex);
    #     menu.$slider.find('li').each ->
    #       # console.log($(this).data('title'));
    #       if $(this).data('title').match(regex)
    #         $(this).show()
    #       else
    #         $(this).hide()

    #   # Mouse / keyboard listeners
    #   @select('menuLoopSelector').on 'click', (event) =>
    #     $(event.currentTarget).toggleClass('selected')
    #     @trigger('TOGGLE_LOOP_REQUESTED')

    #   @select('menuShuffleSelector').on 'click', (event) =>
    #     $(event.currentTarget).toggleClass('selected')
    #     @trigger('TOGGLE_SHUFFLE_REQUESTED')

    #   @select('menuLyricsSelector').on 'click', (event) =>
    #     $(event.currentTarget).toggleClass('selected')
    #     @trigger('TOGGLE_LYRICS_REQUESTED')

    #   @select('trackSelector').on 'click', (event) =>
    #     event.preventDefault()
    #     @loadTrack($(event.currentTarget))

    #   @loadTrack = ($track) ->
    #     if $track.hasClass('disabled')
    #       # Sorry track unavailable message ?
    #       return

    #     if $track.hasClass('selected')
    #       @trigger('PAUSE_REQUESTED')
    #       return

    #     history.pushState {}, '', "/playlists/#{$track.data('track-provider')[0]}#{$track.data('playlist-id')}/tracks/#{$track.data('track-id')}"

    #     data =
    #       provider: $track.data('provider')
    #       videoId: $track.data('video-id')
    #       album_picture: $track.data('album-picture')
    #     @trigger('LOAD_REQUESTED', data)

    #   @select('menuReportSelector').on 'click', (event) =>
    #     event.preventDefault()
    #     $track = $(event.currentTarget)

    #     $currentTrack = @select('currentTrackSelector')
    #     if $currentTrack?
    #       $.post('/video/report', { provider: $currentTrack.data('track-provider'), track_id: $currentTrack.data('track-id') })
    #       .done((data) ->
    #         console.log("Reported: #{$currentTrack.data('track-provider')}/#{$currentTrack.data('track-id')}: #{data.reports} reports!")
    #       )
    #       playerwrapper.nextTrack()

    #   setTimeout(=>
    #     @forward()
    #   , 1000)

    #   $(window).trigger('resize')

    # @resetTimeout = ->
    #   if @autoCloseMenuTimeout?
    #     clearTimeout(@autoCloseMenuTimeout)

    #   unless @$node.hasClass('menu-closed')
    #     @autoCloseMenuTimeout = setTimeout(=>
    #       @trigger('TOGGLE_MENU_REQUESTED')
    #     , 4500)

    # @open = ->
    #   @$node.removeClass('menu-closed')

    # @close = ->
    #   @$node.addClass('menu-closed')

    # @toggle = ->
    #   if @$node.hasClass('menu-closed')
    #     @open()
    #   else
    #     @close()

    # @forward = ->
    #   @select('menuSliderSelector').css('left', -@select('menuSliderSelector').width()/2 + 'px')
    #   @select('menuSliderSelector').removeClass('playlists').addClass('tracks')
    #   @select('menuToolsSelector').removeClass('playlist-panel').addClass('track-panel')
    #   @select('menuSliderSelector').find('.playlist.selected .tracks').removeAttr('style')

    # @backward = ->
    #   @select('menuSliderSelector').css('left', '0px')
    #   @select('menuSliderSelector').removeClass('tracks').addClass('playlists')
    #   @select('menuToolsSelector').removeClass('track-panel').addClass('playlist-panel')

  playlists.attachTo '#playlists'
