# $ ->
#   # This class is the only public interface with a video player.
#   # It can load a player from different providers (Dailymotion & YouTube).
#   player = flight.component ->
#
#     @attributes
#       playButtonSelector: '.playButton'
#
#     @after 'initialize', ->
#       console.log '[player] Initialize'
#       @currentPlayer = null
#       @videoId       = null
#       @options       =
#         lyrics: yes
#         mute: no
#
#       @on document, 'TOGGLE_PLAYBACK_REQUESTED', (event) =>
#         console.log '[player] TOGGLE_PLAYBACK_REQUESTED event received!'
#         @togglePlayback()
#
#       @on document, 'PAUSE_REQUESTED', =>
#         console.log '[player] PAUSE_REQUESTED event received!'
#         @pause()
#
#       @on document, 'LOAD_REQUESTED', (event, data) =>
#         data.options ?= {}
#         console.log '[player] LOAD_REQUESTED event received!', data
#         @load(data.provider, data.videoId, data.options)
#
#       @on document, 'TOGGLE_SHUFFLE_REQUESTED', (event) =>
#         console.log '[player] TOGGLE_SHUFFLE_REQUESTED event received!'
#         @toggleShuffle()
#
#       @on document, 'TOGGLE_LOOP_REQUESTED', (event) =>
#         console.log '[player] TOGGLE_LOOP_REQUESTED event received!'
#         @toggleLoop()
#
#       @on document, 'TOGGLE_LYRICS_REQUESTED', (event) =>
#         console.log '[player] TOGGLE_LYRICS_REQUESTED event received!'
#         @toggleLyrics()
#         @updateLyricsIconAndPlaceholder()
#
#       @loadOptionsFromLocalStorage()
#
#       # FIXME
#       $track = $('.track.selected')
#       if $track.length > 0
#         console.log $track
#         landingPage = if $('.landing-page').length then 1 else 0
#         console.log "landingPage: #{landingPage}"
#         console.log "$track.data: #{$track.data()}"
#         @load($track.data('videoProvider'), $track.data('videoId'), { mute: landingPage, loop: landingPage })
#
#     @loadOptionsFromLocalStorage = ->
#       @options.lyrics = no if localStorage.lyrics is 'false'
#       @options.mute   = yes if localStorage.mute is 'true'
#
#     @load = (provider, videoId, options = {}) ->
#       @videoId = videoId
#
#       console.log "[PLAYER] provider: #{provider}, video ID: #{@videoId}"
#       player = switch provider
#         when 'dailymotion'
#           @dailymotionPlayer ?= new Maldon.Players.Dailymotion('player')
#         when 'you_tube'
#           @youTube ?= new Maldon.Players.YouTube('player')
#
#       unless @currentPlayer?
#         @currentPlayer = player
#
#       if @currentPlayer isnt player
#         @$container.select("#player").remove()
#         @$container.append("<div id='player'></div>")
#         @currentPlayer = player
#
#       @currentPlayer.loadVideo(@videoId, $.extend(@options, options))
#
#     @togglePlayback = ->
#       if @currentPlayer.playing
#         @currentPlayer.pause()
#       else
#         @currentPlayer.play()
#
#     @play = ->
#       @currentPlayer.play()
#
#     @pause = ->
#       @currentPlayer.pause()
#
#     @mute = ->
#       @currentPlayer.mute()
#
#     @toggleShuffle = ->
#       @options.suffle = not @options.suffle
#
#     @toggleLoop = ->
#       @options.loop = not @options.loop
#
#     @toggleLyrics = ->
#       @options.lyrics = not @options.lyrics
#
#   player.attachTo '#player-container'
