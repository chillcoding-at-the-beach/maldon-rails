$ ->
  playerControls = flight.component ->

    @attributes
      previousTrackSelector: '.previous'
      nextTrackSelector: '.next'
      playButtonSelector: '.playback'

    @after 'initialize', ->

      @on document, 'TOGGLE_PLAYBACK_REQUESTED', =>
        console.log '[playerControls] TOGGLE_PLAYBACK_REQUESTED event received!'
        @select('playButtonSelector').toggleClass('icon-pause4').toggleClass('icon-play4')

      @on document, 'CLOSE_MENU_REQUESTED', =>
        console.log '[playerControls] CLOSE_MENU_REQUESTED event received!'
        @$node.addClass('inactive')

      @on document, 'RESET_UI_AUTOHIDE_TIMEOUTS', =>
        @resetTimeout()

      @select('previousTrackSelector').on 'click', =>
        console.log '[playerControls] previousTrackSelector click received!'
        @trigger('PREVIOUS_TRACK_REQUESTED')

      @select('nextTrackSelector').on 'click', =>
        console.log '[playerControls] nextTrackSelector click received!'
        @trigger('NEXT_TRACK_REQUESTED')

      @setInitialState()

    @setInitialState = ->
      @select('playButtonSelector').removeClass('icon-pause4').addClass('icon-play4')

    @resetTimeout = ->
      if @autoCloseControlsTimeout?
        clearTimeout(@autoCloseControlsTimeout)
        @$node.removeClass('inactive')

      @autoCloseControlsTimeout = setTimeout(=>
        @$node.addClass('inactive')
      , 4500)

  playerControls.attachTo '#player-controls'
