$ ->
  videoView = flight.component ->

    @after 'initialize', ->

      @on 'click', =>
        console.log '[videoView] Player overlay clicked', document, 'trigger TOGGLE_PLAYBACK_REQUESTED'
        @trigger('TOGGLE_PLAYBACK_REQUESTED')

  videoView.attachTo '#player-overlay'
