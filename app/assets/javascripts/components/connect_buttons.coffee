$ ->
  connectButtons = flight.component ->

    @attributes
      connectButtonSelector: '.connect'

    @after 'initialize', ->
      console.log '[connectButtons] ', @select('connectButtonSelector')
      @select('connectButtonSelector').on 'click', @connectHandler

    @connectHandler = (event) =>
      console.log '[connectButtons] CLICK event received!'
      event.preventDefault()
      window.location = "/auth/#{$(event.currentTarget).data('service')}"
      # @trigger('PAUSE_REQUESTED')

      no

  connectButtons.attachTo '#connect-buttons'
