$ ->
  mainView = flight.component ->

    @attributes
      menuOpenerSelector: '.menu-icon'

    @after 'initialize', ->

      @on document, 'TOGGLE_MENU_REQUESTED', =>
        console.log '[mainView] TOGGLE_MENU_REQUESTED event received!'
        @$node.toggleClass('menu-closed')

      @select('menuOpenerSelector').on 'click', =>
        console.log '[mainView] menu-icon clicked, TOGGLE_MENU_REQUESTED triggered!'
        @trigger('TOGGLE_MENU_REQUESTED')

  mainView.attachTo '#main-view'
