class TrackVideosFetchingJob < ActiveJob::Base
  queue_as :default

  def perform(track)
    track.fetch_video!
  rescue VideoSearch::NoVideosFound => ex
    Rails.logger.error(ex)
    # todo: push notif
  end
end
