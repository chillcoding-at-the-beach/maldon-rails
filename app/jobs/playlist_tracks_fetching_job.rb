class PlaylistTracksFetchingJob < ActiveJob::Base
  queue_as :default

  def perform(playlist)
    playlist.fetch_tracks!
    # todo: push notif
  end
end
