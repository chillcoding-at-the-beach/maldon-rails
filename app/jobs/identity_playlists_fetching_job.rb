class IdentityPlaylistsFetchingJob < ActiveJob::Base
  queue_as :default

  def perform(identity)
    identity.fetch_playlists!
    # todo: push notif
  end
end
