import BasePlayer from './base'

class DailymotionPlayer extends BasePlayer {
  play() {
    super.play();
    return this._player.play();
  }

  pause() {
    super.pause();
    return this._player.pause();
  }

  mute() {
    super.mute();
    return this._player.setMuted(true);
  }

  unmute() {
    super.unmute();
    return this._player.setMuted(false);
  }

  _loadVideo(videoId) {
    return this._player.load(videoId, this._loadVideoParams());
  }

  _injectJS() {
    var firstScriptTag, tag;
    tag = document.createElement('script');
    tag.async = true;
    tag.src = document.location.protocol + "//api.dmcdn.net/all.js";
    firstScriptTag = document.getElementsByTagName('script')[0];
    return firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  }

  _asyncInit(videoId) {
    window.dmAsyncInit = function() {
      this._player = DM.player(this.playerId, {
        video: videoId,
        width: '100%',
        height: '100%',
        params: this._loadVideoParams()
      });
      return this._attachEventsListeners();
    }.bind(this);
  }

  _loadVideoParams() {
    return {
      autoplay: this.playing,
      mute: this.muted,
      start: true,
      api: true,
      chromeless: true,
      html: true,
      quality: 720,
      loop: this.looping
    }
  }

  _attachEventsListeners() {
    this._player.addEventListener('apiready', this._onApiReady.bind(this));
    this._player.addEventListener('ended', function() {
      return console.log('ended DM !!!');
    });
    // TODO: Make sure the event is dispatched up so that the controls can update accordingly
    // this._player.addEventListener('timeupdate', this.dispatchEvent.bind(this));
    this._player.addEventListener('timeupdate', (event) => {
      this.currentTime = this._player.currentTime
      this.dispatchEvent(new Event('timeupdate'));
    });
  }

  _onApiReady(event) {
    var isiPad;
    isiPad = navigator.userAgent.match(/iPad/i) !== null;
    if (isiPad) {
      console.log('ipad!');
    } else {
      // TODO: Make sure the even is dispatched up so that the controls can update accordingly
      // document.getElementsByTagName('.progress').width(0);
      // this.dispatchEvent(event);
    }
  }
}

export default DailymotionPlayer
