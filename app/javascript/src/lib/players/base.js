class BasePlayer {
  constructor(playerId) {
    this.playerId = playerId;
    this.eventTarget = new EventTarget();
    this.currentTime = 0;
    this.playing = false;
    this.muted = false;
    this.looping = false;
  }

  addEventListener(type, callback) {
    this.eventTarget.addEventListener(type, callback);
  }

  removeEventListener(type, callback) {
    this.eventTarget.removeEventListener(type, callback);
  }

  dispatchEvent(event) {
    this.eventTarget.dispatchEvent(event);
  }

  loadVideo(videoId, options) {
    if (options == null) {
      options = {};
    }

    if (options.autoplay != null) { this.playing =  options.autoplay; }
    if (options.mute != null) { this.muted =  options.mute; }
    if (options.loop != null) { this.looping =  options.loop; }

    if (this._player != null) {
      return this._loadVideo(videoId);
    } else {
      return this._init(videoId);
    }
  }

  play() {
    return this.playing = true;
  }

  pause() {
    return this.playing = false;
  }

  togglePlay() {
    if (this.playing) {
      this.pause();
    } else {
      this.play();
    }
  }

  mute() {
    return this.muted = true;
  }

  unmute() {
    return this.muted = false;
  }

  toggleMute() {
    if (this.muted) {
      this.unmute();
    } else {
      this.mute();
    }
  }

  _init(videoId) {
    // $('.playButton').removeClass('icon-pause4').addClass('icon-play4');
    this._injectJS();
    return this._asyncInit(videoId);
  }
}

export default BasePlayer
