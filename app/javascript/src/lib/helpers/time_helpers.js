export function prettyDuration(durationInSeconds) {
  if (isNaN(durationInSeconds) || !isFinite(durationInSeconds)) {
    return '??:??:??'
  }
  var seconds, minutesTotal, minutes, hours, string;

  seconds = Math.trunc(durationInSeconds % 60)
  minutesTotal = Math.trunc(durationInSeconds / 60)
  minutes = Math.trunc(minutesTotal % 60)
  hours = Math.trunc(minutesTotal / 60)

  string = []
  if (hours > 0) {
    string = string.concat(hours < 10 ? "0" + hours : hours)
  }
  // if (minutes > 0) {
    string = string.concat(minutes < 10 ? "0" + minutes : minutes)
  // }
  string = string.concat(seconds < 10 ? "0" + seconds : seconds)

  return string.join(':')
}

//
// export function timeFormat(durationInSeconds) {
//   if (isNaN(durationInSeconds) || !isFinite(durationInSeconds)) {
//     return '--:--';
//   }
//   var hours, minutes, seconds;
//
//   durationInSeconds = Math.round(durationInSeconds);
//   hours = Math.floor(seconds / 3600);
//   hours = hours < 10 ? '0' + hours : hours;
//
//   minutes = Math.floor(seconds / 60) % 60;
//   minutes = minutes < 10 ? '0' + minutes : minutes;
//
//   seconds = seconds % 60;
//   seconds = seconds < 10 ? '0' + seconds : seconds;
//
//   if (durationInSeconds < 3600) {
//     return minutes + ':' + seconds;
//   } else {
//     return hours + 'h' + minutes;
//   }
// }
