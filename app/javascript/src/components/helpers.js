/* eslint no-console:0 */
import 'core-js/stable'
import 'regenerator-runtime/runtime'
import Vue from 'vue/dist/vue.esm'

const files = require.context('./', true, /\.vue$/i)
files.keys().map(key => {
  const component = key.split('/').pop().split('.')[0]
  // With Lazy Loading
  Vue.component(component, () => import(`${key}`))
})
