import '../src/components/helpers'
import Vue from 'vue/dist/vue.esm'

require.context('../images', true)

import '../stylesheets/application.scss'
// import '../images/umpty.png'
import UmptyImage from 'images/umpty.png'
import { prettyDuration } from '../src/lib/helpers/time_helpers'

document.addEventListener('DOMContentLoaded', () => {
  var app = new Vue({
    el: '#playlists-page',
    data: {
      playlists: [],
      coverImageUrl: UmptyImage,
      coverOverlayUrl: UmptyImage,
      currentPlaylist: null,
    },
    computed: {
      currentPlaylistTitle () {
        if (this.currentPlaylist === null) {
          return "No playlist"
        }

        return this.currentPlaylist.title
      },
      currentPlaylistTracksCount () {
        if (this.currentPlaylist === null) {
          return 0
        }

        return this.currentPlaylist.tracks.length
      },
      currentPlaylistDuration () {
        if (this.currentPlaylist === null) {
          return prettyDuration(null)
        }

        prettyDuration(this.currentPlaylist.duration)
      },
    },
    created () {
      fetch('/playlists.json')
      .then(response => response.json())
      .then(json => {
        this.playlists = json.playlists

        if (this.playlists.length > 0) {
          this.currentPlaylist = this.playlists[0]
          this.coverOverlayUrl = this.currentPlaylist.picture_url

          if (this.currentPlaylist.tracks.length > 0) {
            this.coverImageUrl = this.currentPlaylist.tracks[0].album_image_url
          }
        }
      })
    },
  })
})
