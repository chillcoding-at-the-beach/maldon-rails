import '../src/components/helpers'
import Vue from 'vue/dist/vue.esm'

import '../stylesheets/application.scss'
require.context('../images', true)

document.addEventListener('DOMContentLoaded', () => {
  new Vue({
    el: '#landing-vue',
    data: {
      playlist: { tracks: [{ video: { ref: 'xwr14q', provider: { name: 'dailymotion' } } }] },
    },
    template: '<player v-bind:playlist="playlist" v-bind:show-controls="false" v-bind:player-options="{ autoplay: true, mute: true }"></player>'
  })
})
