class TracksController < ApplicationController
  before_action :login_required

  def show
    redirect_to playlists_path
  end
end
