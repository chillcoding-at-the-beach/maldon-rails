class LandingController < ApplicationController
  def index
    redirect_to playlists_path if signed_in?

    respond_to do |format|
      format.html
      format.json do
        jsonPayload = { playlists: [{ tracks: [{ video: { ref: 'xwr14q', provider: { name: 'dailymotion' } } }] }] }
        render json: jsonPayload
      end
    end
  end
end
