class SessionsController < ApplicationController
  def demo
    self.current_user = User.demo_user
    redirect_to playlists_path, notice: "Signed in!"
  end

  def create
    # @user = User.find_or_create_from_auth_hash(auth_hash)
    # self.current_user = @user
    # redirect_to '/'

    # Find an identity here
    @identity = Identity.find_with_omniauth(auth_hash)

    # Not signed-in, let's find or create the user and sign him up
    if !signed_in?
      user = @identity.nil? ? User.find_or_create_from_auth_hash!(auth_hash) : @identity.user
      self.current_user = user
    end

    # No identity let's create a new identity for the current user!
    if @identity.nil?
      @identity = Identity.create_with_omniauth_for_user!(auth_hash, current_user)
    end

    # if signed_in?
      if @identity.user == current_user
        # User is signed in so they are trying to link an identity with their
        # account. But we found the identity and the user associated with it
        # is the current user. So the identity is already associated with
        # this user. So let's display an error message.
        redirect_to playlists_path, notice: "Already linked that account!"
      else
        # The identity is not associated with the current_user so lets
        # associate the identity
        associate_identity_with_current_user_and_redirect
      end
    # else
    #   # if @identity.user.present?
    #     # The identity we found had a user associated with it so let's
    #     # just log them in here
    #     self.current_user = @identity.user
    #     redirect_to playlists_path, notice: "Signed in!"
    #   # else
    #   #   # No user associated with the identity so we need to create a new one
    #   #   user = User.find_or_create_from_auth_hash!(auth_hash)
    #   #   self.current_user = user
    #   #   associate_identity_with_current_user_and_redirect
    #   # end
    # end
  end

  def destroy
    self.current_user = nil
    redirect_to root_url, notice: "Signed out!"
  end

  protected

  def auth_hash
    request.env['omniauth.auth']
  end

  def associate_identity_with_current_user_and_redirect
    @identity.user = current_user
    @identity.save
    redirect_to playlists_path, notice: "Successfully linked that account!"
  end
end
