class PlaylistsController < ApplicationController
  before_action :login_required

  def index
    respond_to do |format|
      format.html
      # format.xml  { render xml: @users }
      format.json do
        jsonPayload = { playlists: serialize_playlists(current_user.playlists_for_provider('deezer')) + serialize_playlists(current_user.playlists_for_provider('spotify')) }
        render json: jsonPayload
      end
    end
  end

  private

  def serialize_playlists(playlists)
    playlists.as_json(
      only: [:title, :ref, :picture_url, :duration],
      include: [
        provider: {
          only: :name
        },
        tracks: {
          only: [:title, :artist_name, :duration, :ref, :artist_image_url, :album_image_url],
          include: {
            video: {
              only: :ref,
              include: {
                provider: {
                  only: :name
                }
              }
            }
          }
        }
      ]
    )
  end
end
