class Lyric < ApplicationRecord
  belongs_to :track, inverse_of: :lyric

  validates :payload, presence: true
  validates :offset, numericality: { only_integer: true }
end
