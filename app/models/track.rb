class Track < ApplicationRecord
  belongs_to :provider, class_name: 'TrackProvider', foreign_key: :track_provider_id, inverse_of: :tracks
  has_and_belongs_to_many :playlists
  belongs_to :video, inverse_of: :tracks, optional: true
  has_one :lyric, dependent: :delete

  validates :ref,
            :title,
            presence: true

  def self.find_or_create_from_api_result_and_identity!(result, identity)
    create_with(result).find_or_create_by!(
      provider: identity.provider,
      ref: result[:ref]
    )
  end

  def fetch_video!
    return video if video.present?

    if video_attrs = VideoSearch.new(self).search
      Rails.logger.info(video_attrs.inspect)

      self.video = Video.find_or_create_from_video_search_and_track!(video_attrs)
      self.save!
    end
  end
end
