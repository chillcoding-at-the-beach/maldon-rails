class Video < ApplicationRecord
  belongs_to :provider, class_name: 'VideoProvider', foreign_key: :video_provider_id, inverse_of: :videos
  has_many :tracks, inverse_of: :video

  validates :ref,
            presence: true

  def self.find_or_create_from_video_search_and_track!(attrs)
    find_attrs = attrs.slice(:provider, :ref)
    create_with(attrs).find_or_create_by!(find_attrs)
  end
end
