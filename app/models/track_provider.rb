class TrackProvider < ApplicationRecord
  has_many :identities, inverse_of: :provider
  has_many :tracks, inverse_of: :provider
  has_many :playlists, inverse_of: :playlists

  validates :name, presence: true
end
