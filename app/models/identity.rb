class Identity < ApplicationRecord
  belongs_to :provider, class_name: 'TrackProvider', foreign_key: :track_provider_id, inverse_of: :identities
  belongs_to :user, inverse_of: :identities
  has_many :playlists, -> { includes(:provider, tracks: { video: :provider }) }
  has_many :tracks, -> { includes(:provider) }, through: :playlists

  validates :uid, :access_token, presence: true

  def self.find_with_omniauth(auth)
    find_by(
      provider: TrackProvider.find_by!(name: auth['provider']),
      uid: auth['uid'],
    )
  end

  def self.create_with_omniauth_for_user!(auth, user)
    create!(
      provider: TrackProvider.find_by!(name: auth['provider']),
      uid: auth['uid'],
      access_token: auth['credentials']['token'],
      user: user
    )
  end

  def playlists!(remote_fetch = false)
    if remote_fetch || playlists.empty?
      IdentityPlaylistsFetchingJob.perform_later(self)
      []
    else
      playlists.includes(:provider, :tracks)
    end
  end

  # Called from IdentityPlaylistsFetchingJob
  def fetch_playlists!
    raw_playlists = provider_class.new(access_token).user_playlists(uid)

    raw_playlists.map do |raw_playlist|
      playlist = Playlist.find_or_create_from_api_result_and_identity!(raw_playlist, self)
      PlaylistTracksFetchingJob.perform_later(playlist)
    end
  end

  private

  def provider_class
    @provider_class ||= provider.name.camelize.constantize
  end
end
