class Playlist < ApplicationRecord
  enum access: %i[private public], _suffix: true

  belongs_to :identity, inverse_of: :playlists
  belongs_to :provider, class_name: 'TrackProvider', foreign_key: :track_provider_id, inverse_of: :playlists
  has_and_belongs_to_many :tracks, -> { includes(:video) }

  validates :ref,
            :title,
            :original_url,
            presence: true
  validates :ref, uniqueness: { scope: :track_provider_id }

  def self.find_or_create_from_api_result_and_identity!(result, identity)
      playlist = Playlist.create_with(result.merge(identity: identity)).find_or_create_by!(
        provider: identity.provider,
        ref: result[:ref]
      )
  end

  def fetch_tracks!
    raw_tracks = provider_class.new(identity.access_token).playlist_tracks(identity.uid, ref).reverse
    track_ids_with_existing_videos = Track.where(provider: identity.provider, ref: raw_tracks.map { |t| t[:ref] }).where.not(video_id: nil).pluck(:id)

    raw_tracks.map do |raw_track|
      track = Track.find_or_create_from_api_result_and_identity!(raw_track, identity)

      self.tracks << track unless tracks.include?(track)
      TrackVideosFetchingJob.perform_later(track) unless track_ids_with_existing_videos.include?(track.id)
    end
    self.save!
  end

  def self.select_first_playlist(playlists)
    unless first_playlist = playlists.detect { |p| p.default && p.tracks.size > 0 }
      first_playlist = playlists.detect { |p| p.tracks.size > 0 }
    end

    first_playlist
  end

  private

  def provider_class
    @provider_class ||= provider.name.camelize.constantize
  end
end
