class User < ApplicationRecord
  enum gender: %i[unknown female male]

  has_many :identities, inverse_of: :user
  has_many :playlists, through: :identities
  has_many :tracks, through: :playlists
  has_many :videos, through: :tracks

  validates :login, :email, presence: true, uniqueness: true
  validates :country, :lang, length: { is: 2 }, allow_nil: true

  DEMO_EMAIL = 'try.maldon@gmail.com'
  GENDERS = {
    u: :unknown,
    f: :female,
    m: :male
  }

  def self.find_or_create_from_auth_hash!(auth)
    name = auth['info']['name'].split(' ')
    find_or_create_by!(email: auth['info']['email']) do |user|
      user.login       = auth['info']['nickname']
      user.first_name  = auth['info']['first_name'] || name.shift()
      user.last_name   = auth['info']['last_name'] || name.join(' ')
      user.picture_url = auth['info']['image']
      # TODO: Use an enum
      user.gender      = gender_from_auth_hash(auth)
      user.country     = auth['extra']['raw_info']['country'].downcase rescue nil
      user.lang        = auth['extra']['raw_info']['lang'].downcase rescue nil
    end
  end

  def self.gender_from_auth_hash(hash)
    letter = hash.dig('extra', 'raw_info', 'gender').presence || 'u'

    GENDERS[letter.downcase.to_sym]
  end

  def self.demo_user
    create_with(login: 'demo', first_name: 'Demo', last_name: 'User').find_or_create_by(email: DEMO_EMAIL)
  end

  def playlists_for_provider(provider)
    identity = identities.find_by(provider: TrackProvider.find_by(name: provider))

    if identity
      identity.playlists!
    else
      []
    end
  end
end
