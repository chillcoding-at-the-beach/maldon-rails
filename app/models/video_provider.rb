class VideoProvider < ApplicationRecord
  validates :name, presence: true

  has_many :videos, inverse_of: :provider
end
