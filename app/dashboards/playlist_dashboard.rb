require "administrate/base_dashboard"

class PlaylistDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    identity: Field::BelongsTo,
    provider: Field::BelongsTo.with_options(class_name: "TrackProvider"),
    tracks: Field::HasMany,
    id: Field::Number,
    track_provider_id: Field::Number,
    ref: Field::String,
    title: Field::String,
    description: Field::Text,
    duration: Field::Number,
    original_url: Field::String,
    picture_url: Field::String,
    access: Field::String.with_options(searchable: false),
    default: Field::Boolean,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = %i[
    id
    provider
    identity
    title
    tracks
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = %i[
    identity
    provider
    tracks
    description
    duration
    original_url
    picture_url
    default
    created_at
    updated_at
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = %i[
    identity
    provider
    tracks
    track_provider_id
    ref
    title
    description
    duration
    original_url
    picture_url
    access
    default
  ].freeze

  # COLLECTION_FILTERS
  # a hash that defines filters that can be used while searching via the search
  # field of the dashboard.
  #
  # For example to add an option to search for open resources by typing "open:"
  # in the search field:
  #
  #   COLLECTION_FILTERS = {
  #     open: ->(resources) { resources.where(open: true) }
  #   }.freeze
  COLLECTION_FILTERS = {}.freeze

  # Overwrite this method to customize how playlists are displayed
  # across all pages of the admin dashboard.
  #
  def display_resource(playlist)
    "Playlist ##{playlist.id} / #{playlist.provider.name}##{playlist.ref} - #{playlist.title} (#{playlist.access})"
  end
end
