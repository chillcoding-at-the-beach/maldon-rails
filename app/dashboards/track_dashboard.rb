require "administrate/base_dashboard"

class TrackDashboard < Administrate::BaseDashboard
  # ATTRIBUTE_TYPES
  # a hash that describes the type of each of the model's fields.
  #
  # Each different type represents an Administrate::Field object,
  # which determines how the attribute is displayed
  # on pages throughout the dashboard.
  ATTRIBUTE_TYPES = {
    provider: Field::BelongsTo.with_options(class_name: "TrackProvider"),
    playlists: Field::HasMany,
    video: Field::BelongsTo,
    lyric: Field::HasOne,
    id: Field::Number,
    track_provider_id: Field::Number,
    ref: Field::String,
    artist_name: Field::String,
    artist_image_url: Field::String,
    title: Field::String,
    isrc: Field::String,
    original_url: Field::String,
    album_title: Field::String,
    album_image_url: Field::String,
    duration: Field::Number,
    explicit_lyrics: Field::Boolean,
    bpm: Field::Number,
    reports: Field::Number,
    created_at: Field::DateTime,
    updated_at: Field::DateTime,
    video_id: Field::Number,
  }.freeze

  # COLLECTION_ATTRIBUTES
  # an array of attributes that will be displayed on the model's index page.
  #
  # By default, it's limited to four items to reduce clutter on index pages.
  # Feel free to add, remove, or rearrange items.
  COLLECTION_ATTRIBUTES = %i[
    id
    provider
    title
    artist_name
    video
    playlists
    lyric
  ].freeze

  # SHOW_PAGE_ATTRIBUTES
  # an array of attributes that will be displayed on the model's show page.
  SHOW_PAGE_ATTRIBUTES = %i[
    provider
    playlists
    video
    lyric
    id
    track_provider_id
    ref
    artist_name
    artist_image_url
    title
    isrc
    original_url
    album_title
    album_image_url
    duration
    explicit_lyrics
    bpm
    reports
    created_at
    updated_at
    video_id
  ].freeze

  # FORM_ATTRIBUTES
  # an array of attributes that will be displayed
  # on the model's form (`new` and `edit`) pages.
  FORM_ATTRIBUTES = %i[
    provider
    playlists
    video
    lyric
    track_provider_id
    ref
    artist_name
    artist_image_url
    title
    isrc
    original_url
    album_title
    album_image_url
    duration
    explicit_lyrics
    bpm
    reports
    video_id
  ].freeze

  # COLLECTION_FILTERS
  # a hash that defines filters that can be used while searching via the search
  # field of the dashboard.
  #
  # For example to add an option to search for open resources by typing "open:"
  # in the search field:
  #
  #   COLLECTION_FILTERS = {
  #     open: ->(resources) { resources.where(open: true) }
  #   }.freeze
  COLLECTION_FILTERS = {}.freeze

  # Overwrite this method to customize how tracks are displayed
  # across all pages of the admin dashboard.
  #
  # def display_resource(track)
  #   "Track ##{track.id}"
  # end
end
