class Dailymotion
  include HTTParty
  base_uri 'https://api.dailymotion.com'
  # debug_output $stderr

  def initialize(options = {})
    @options = options
  end

  def videos
    self.class.get('/videos', options)
  end

  private

  attr_reader :options
end
