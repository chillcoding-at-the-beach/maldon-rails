class YouTube
  include HTTParty
  base_uri 'https://www.googleapis.com/youtube/v3'
  # debug_output $stderr

  QuotaExceededError = Class.new(StandardError)

  def initialize(options = {})
    @options = options
  end

  def videos
    self.class.get('/search', options)
  end

  private

  attr_reader :options

  def handle_response(response)
    case response.code
    when 200...300
      response
    when 403
      raise QuotaExceededError, response.message
    end
  end
end
