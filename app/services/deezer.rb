class Deezer
  include HTTParty
  base_uri 'https://api.deezer.com'
  # debug_output $stderr

  def initialize(access_token = nil)
    @options = {}
    @options[:access_token] = access_token if access_token
  end

  def user_playlists(user_id)
    _user_playlists(user_id).map do |playlist|
      {
        ref:          playlist['id'],
        title:        playlist['title'],
        duration:     playlist['duration'],
        description:  playlist['description'],
        original_url: playlist['link'],
        picture_url:  playlist['picture'],
        access:       (playlist['public'] ? Playlist.accesses[:public] : Playlist.accesses[:private]),
        default:      playlist['is_loved_track']
      }
    end
  end

  def playlist_tracks(user_id, playlist_id)
    _playlist_tracks(user_id, playlist_id).map do |track|
      {
        ref:              track['id'],
        artist_name:      track['artist']['name'],
        artist_image_url: track['artist']['picture'],
        title:            track['title'],
        duration:         track['duration'], # We want the duration in milliseconds
        isrc:             track['isrc'],
        original_url:     track['link'],
        explicit_lyrics:  track['explicit_lyrics'],
        bpm:              track['bpm'],
        album_title:      track['album']['title'],
        album_image_url:  track['album']['cover']
      }
    end
  end

  def track(track_id)
    self.class.get("/track/#{track_id}", options)
  end

  private

  attr_reader :options

  def _user_playlists(user_id)
    self.class.get("/user/#{user_id}/playlists", options)['data']
  end

  def _playlist_tracks(user_id, playlist_id)
    self.class.get("/playlist/#{playlist_id}/tracks", options)['data']
  end
end
