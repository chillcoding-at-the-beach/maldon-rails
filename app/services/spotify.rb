class Spotify
  include HTTParty
  base_uri 'https://api.spotify.com'
  # debug_output $stderr

  def initialize(access_token = nil)
    @options = { headers: {} }
    @options[:headers]['Authorization'] = "Bearer #{access_token}" if access_token
  end

  def user_playlists(user_id)
    playlists = self.class.get("/v1/users/#{user_id}/playlists", options)

    playlists['items'].map do |playlist|
      {
        ref:  playlist['id'],
        title:        playlist['name'],
        duration:     nil, # FIXME
        description:  nil, # FIXME
        original_url: playlist['external_urls']['spotify'],
        picture_url:  (playlist['images'][0]['url'] rescue nil),
        access:       (playlist['public'] ? Playlist.accesses[:public] : Playlist.accesses[:private]),
        default:      false # FIXME
      }
    end
  end

  def playlist(user_id, playlist_id)
    tracks = self.class.get("/v1/users/#{user_id}/playlists/#{playlist_id}/tracks", options)

    tracks['items'].map do |data|
      map_track(data['track'])
    end.compact
  end

  def track(track_id)
    map_track self.class.get("/v1/tracks/#{track_id}", options)
  end

  private

  attr_reader :options

  def map_track(track)
      return nil unless track['type'] == 'track'

      {
        ref:      track['id'],
        artist_name:      track['artists'][0]['name'], # FIXME: Handle multiple artists?
        artist_image_url: nil,
        title:            track['name'],
        duration:         track['duration_ms'],
        isrc:             track['external_ids']['isrc'],
        original_url:     track['external_urls']['spotify'],
        explicit_lyrics:  track['explicit'],
        bpm:              nil,
        album_title:      track['album']['name'],
        album_image_url:  track['album']['images'][0]['url'] # FIXME: Ensure we get the biggest image?
      }
  end
end
