class VideoSearch
  NoVideosFound = Class.new(StandardError)

  def initialize(track)
    @track = track
  end

  def search
    @video = fulltext_search_dailymotion# || fulltext_search_youtube
  end

  def fulltext_search_dailymotion
    opts = {
      query: {
        channel: 'music',
        filters: 'no-live,no-premium',
        sort: 'relevance',
        search: _clean_fulltext,
        fields: %w(id aspect_ratio title explicit isrc views_total)
      }
    }
    videos = Dailymotion.new(opts).videos['list']

    videos_with_isrc, other_videos = sort_by_views_and_partitions_by_isrc(videos)
    selected_video = videos_with_isrc.first || other_videos.first

    if selected_video
      Rails.logger.info selected_video.inspect
      return {
        ref: selected_video['id'],
        title: selected_video['title'],
        aspect_ratio: selected_video['aspect_ratio'],
        provider: VideoProvider.find_by!(name: 'dailymotion')
      }
    end

    raise(NoVideosFound, "No videos found for '#{_clean_fulltext}': #{videos}")
  end

  def sort_by_views_and_partitions_by_isrc(videos)
    videos.sort_by { |video| -video['views_total'] }.partition { |video| video['isrc'] }
  end

  def fulltext_search_youtube
    opts = {
      query: {
        part: 'snippet',
        maxResults: 25,
        type: 'video',
        videoDefinition: 'high',
        videoCategoryId: 10,
        order: 'relevance',
        key: ENV.fetch('GOOGLE_KEY'),
        q: _clean_fulltext
      }
    }
    videos = YouTube.new(opts).videos
    raise(NoVideosFound, "No videos found for '#{_clean_fulltext}': #{videos.inspect}") unless videos['items']

    video = videos['items'].first
    raise(NoVideosFound, "First video is nil for '#{_clean_fulltext}': #{videos['items'].inspect}") unless video

    {
      ref: video['id']['videoId'],
      title: video['snippet']['title'],
      aspect_ratio: 1.77778,
      provider: VideoProvider.find_by!(name: 'youtube')
    }
  rescue YouTube::QuotaExceededError => ex
    raise(NoVideosFound, "Quota exceeded for '#{_clean_fulltext}': #{ex.inspect}")
  end

  private

  attr_reader :track

  def _fulltext
    @_fulltext ||= "#{track.artist_name} #{track.title}"
  end

  def _clean_fulltext
    _fulltext.remove(/\s*\([^()]*\)\s*/)
  end
end
