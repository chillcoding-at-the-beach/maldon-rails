module TimeHelper
  def duration_string(duration)
    return '??:??:??' if duration.blank?

    seconds = duration % 60
    minutes_tot = duration / 60
    minutes = minutes_tot % 60
    hours = minutes_tot / 60

    string = []
    string << (hours < 10 ? "0#{hours}" : hours) if hours > 0
    string << (minutes < 10 ? "0#{minutes}" : minutes)
    string << (seconds < 10 ? "0#{seconds}" : seconds)

    string.join(':')
  end
end
