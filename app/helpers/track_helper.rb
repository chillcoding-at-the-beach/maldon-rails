module TrackHelper
  def data_attributes_for_track(track, playlist)
    attributes = {
      :'video-provider' => track.video.provider.name,
      :'video-id'       => track.video.ref,
      :'track-provider' => track.provider.name,
      :'track-id'       => track.ref,
      :'artist-picture' => track.artist_image_url,
      :'album-picture'  => track.album_image_url,
      title: track.title,
      artist: track.artist_name,
      :'playlist-id'    => playlist.ref
    }

    attributes.map do |key, value|
      "data-#{key}='#{value}'"
    end.join(' ').html_safe
  end
end
